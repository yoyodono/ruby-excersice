# For loop
villa = "Villa nya DONO neh"
villa_dono = "DONO yang keren itu lho"

if villa == villa_dono
  puts "berarti dono itu ganteng"
else
  puts "dono tetap saja ganteng"
end

# times loop (very simple though)
times_loop = 10
i = 1
times_loop.times do
  i += 1
end
puts i


# looping with function
class Dono
  def self.loop
    puts "Dono Ganteng kalau ini berhasil"
  end
  def self.loop2(loop)
    times_loop = 10
    i = 1
    times_loop.times do
      puts loop
    end
  end
end

puts Dono.loop
puts Dono.loop2("Dono Kueren")

# Instance variable
class WajahDono
  class << self; attr_accessor :wajah end
  @wajah = "Ganteng"
end

puts "Wajah Dono itu #{WajahDono.wajah}"
