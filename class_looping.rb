# Looping nya di luar class dan manggil method class nya
class Dono
  def self.loop(value)
    #puts "Dono #{value}"
  end
end

counter = 1
5.times do 
  Dono.loop(counter)
  counter += 1
end

# Looping nya di dalam class
class Maria
  def self.latihan(counter2)
    5.times do
      puts "Dono #{counter2}"
      counter2 += 1
    end
  end
end

counter2 = 10
Maria.latihan(counter2)
